# Pillar Code Kata - Vending Machine #

Kata Location: https://github.com/guyroyse/vending-machine-kata

Since this is just a basic example the majority of tools that would normally be employed
during actual implementation have been omitted: Spring IoC, i18n currency types, logging, 
message resources bundles, externalized configuration, service calls, persistence, etc. 

Along with above, there is no use of factories, commands, DAOs, or other patterns that would be present in a full-fledged implementation.

This exercise demonstrates functional code that satisfies all of the requirements and has been developed in a manner consistent with TDD.

# Notes #
1. *There is no UI* - this project is simply the brains of a vending machine and the tests to support that it functions correctly. Running *mvn test* at its root will kick off the test suite.

2. The requirement for *Exact Change* can't be completely satisfied as written as we can't determine if exact change is needed outside of an initial state of having no currency in the machine. Once change has been inserted we can then determine if exact change will be needed.

3. There was no requirement for adding currency to the machine so this has been implied and tests created.

4. There was no requirement for adding inventory to the machine so this has been implied and tests created.

5. The *Accept Coins* requirement specifies that invalid coins are returned and returned via the coin slot. This is implied in the code in that the coin is not added to its currency inventory and this display still reads INSERT COIN.

6. As of yet I haven't added Cucumber-JVM acceptance tests. Perhaps I'll do that in the morning before this is reviewed...