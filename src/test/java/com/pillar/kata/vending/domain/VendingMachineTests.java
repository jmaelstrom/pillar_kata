package com.pillar.kata.vending.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class VendingMachineTests {
	VendingMachine vendingMachine;
	
	DecimalFormat decimalFormat = new DecimalFormat("#0.##");

	@Before
	public void setup() {
		vendingMachine = new VendingMachine();
	}

	@Test
	public void addInventoryToVendingMachine() {
		assertEquals(new Integer(5), vendingMachine.addInventory("starkists", new BigDecimal(.5), 5));
	}

	@Test
	public void queryExistingInventoryInVendingMachineByName() {
		assertEquals(new Integer(5), vendingMachine.addInventory("starkists", new BigDecimal(.5), 5));
		assertEquals(new Integer(5), vendingMachine.checkInventoryCount("starkists"));
	}

	@Test
	public void queryNonExistingInventoryInVendingMachineByNameShouldReturnZero() {
		assertEquals(new Integer(5), vendingMachine.addInventory("starkists", new BigDecimal(.5), 5));
		assertEquals(new Integer(0), vendingMachine.checkInventoryCount("rollos"));
	}

	@Test
	public void addCurrencyDenominationToVendingMachine() {
		assertEquals(new Integer(10), vendingMachine.addCurrencyInventory("penny", 10, .1f, .75f, .06f, new BigDecimal(.01)));
		assertEquals(new Integer(10), vendingMachine.addCurrencyInventory("penny", VendingMachine.penny, 10));
	}

	@Test
	public void queryExistingCurrencyDenominationInventoryInVendingMachineByName() {
		assertEquals(new Integer(10), vendingMachine.addCurrencyInventory("penny", 10, .1f, .75f, .06f, new BigDecimal(.01)));
		assertEquals(new Integer(10), vendingMachine.checkCurrencyDenominationInventoryCount("penny"));

		assertEquals(new Integer(10), vendingMachine.addCurrencyInventory("penny", VendingMachine.penny, 10));
		assertEquals(new Integer(10), vendingMachine.checkCurrencyDenominationInventoryCount("penny"));
	}

	@Test
	public void queryNonExistingCurrencyDenominationInventoryInVendingMachineByName() {
		assertEquals(new Integer(10), vendingMachine.addCurrencyInventory("penny", 10, .1f, .75f, .06f, new BigDecimal(.01)));
		assertEquals(new Integer(0), vendingMachine.checkCurrencyDenominationInventoryCount("nickel"));

		assertEquals(new Integer(10), vendingMachine.addCurrencyInventory("penny", VendingMachine.penny, 10));
		assertEquals(new Integer(0), vendingMachine.checkCurrencyDenominationInventoryCount("nickel"));
	}

	@Test
	public void checkVendingDisplayWhenNoCoinsInserted() {
		vendingMachine.addCurrencyInventory(VendingMachine.quarter.getName(), VendingMachine.quarter, 1);
		
		assertEquals(VendingMachine.INSERT_COIN, vendingMachine.displayMessage());
	}

	@Test
	public void acceptCoin() {
		assertTrue(vendingMachine.acceptCurrencyDenomination(VendingMachine.nickel));
	}

	@Test
	public void acceptValidCoinNotPenny() {
		assertTrue(vendingMachine.acceptCurrencyDenomination(VendingMachine.nickel));
		assertFalse(vendingMachine.acceptCurrencyDenomination(VendingMachine.penny));
	}
	
	@Test
	public void notAcceptInvalidCurrency() {
		CurrencyDenomination fakeQuarter = new CurrencyDenomination("quarter", 6.67f, 24.26f, 1.75f,
				new BigDecimal(.25).setScale(2, RoundingMode.HALF_UP));
		assertFalse(vendingMachine.acceptCurrencyDenomination(fakeQuarter));
		
	}

	@Test
	public void acceptValidCoinNotPennyAndUpdateDisplay() {
		// must provide some change to machine to avoid EXACT_CHANGE situation 
		vendingMachine.addCurrencyInventory(VendingMachine.quarter.getName(), VendingMachine.quarter, 1);
		
		assertFalse(vendingMachine.acceptCurrencyDenomination(VendingMachine.penny));
		assertEquals(VendingMachine.INSERT_COIN, vendingMachine.displayMessage());

		assertTrue(vendingMachine.acceptCurrencyDenomination(VendingMachine.nickel));
		assertEquals(decimalFormat.format(VendingMachine.nickel.getValue()), vendingMachine.displayMessage());
	}

	@Test
	public void selectAvailableInventoryProductWhenExactCurrencyIsInserted() {
		vendingMachine.addInventory("cola", new BigDecimal(1), 5);
		vendingMachine.addInventory("chips", new BigDecimal(.5), 5);
		vendingMachine.addInventory("candy", new BigDecimal(.65), 5);

		assertEquals(new Integer(5), vendingMachine.checkInventoryCount("candy"));

		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);

		assertTrue(vendingMachine.selectProduct("cola"));
		assertEquals(VendingMachine.THANK_YOU, vendingMachine.displayMessage());
		assertEquals(VendingMachine.INSERT_COIN, vendingMachine.displayMessage());

	}

	@Test
	public void selectAvailableInventoryProductWhenNotEnoughCurrencyIsInserted() {
		vendingMachine.addInventory("cola", new BigDecimal(1), 5);
		vendingMachine.addInventory("chips", new BigDecimal(.5), 5);
		vendingMachine.addInventory("candy", new BigDecimal(.65), 5);

		assertEquals(new Integer(5), vendingMachine.checkInventoryCount("candy"));

		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);

		assertFalse(vendingMachine.selectProduct("cola"));
		assertEquals(VendingMachine.PRICE_PREFIX + decimalFormat.format(new BigDecimal(1)), vendingMachine.displayMessage());

		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		assertTrue(vendingMachine.selectProduct("cola"));
		assertEquals(VendingMachine.THANK_YOU, vendingMachine.displayMessage());
		assertEquals(VendingMachine.INSERT_COIN, vendingMachine.displayMessage());

	}

	@Test
	public void selectAvailableInventoryProductWhenInitiallyNotEnoughCurrencyIsInserted() {
		vendingMachine.addInventory("cola", new BigDecimal(1), 5);
		vendingMachine.addInventory("chips", new BigDecimal(.5), 5);
		vendingMachine.addInventory("candy", new BigDecimal(.65), 5);

		assertEquals(new Integer(5), vendingMachine.checkInventoryCount("candy"));

		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);

		assertFalse(vendingMachine.selectProduct("cola"));
		assertEquals(VendingMachine.PRICE_PREFIX + decimalFormat.format(new BigDecimal(1)), vendingMachine.displayMessage());

		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		assertTrue(vendingMachine.selectProduct("cola"));
		assertEquals(VendingMachine.THANK_YOU, vendingMachine.displayMessage());
		assertEquals(VendingMachine.INSERT_COIN, vendingMachine.displayMessage());

	}

	@Test
	public void selectAvailableInventoryProductWhenMoreThanRequiredCurrencyIsInsertedAndChangeExists() {
		assertEquals(new Integer(2), vendingMachine.addCurrencyInventory(VendingMachine.quarter.getName(), VendingMachine.quarter, 2));
		assertEquals(new Integer(1), vendingMachine.addCurrencyInventory(VendingMachine.nickel.getName(), VendingMachine.nickel, 1));
		
		vendingMachine.addInventory("cola", new BigDecimal(1), 5);
		vendingMachine.addInventory("chips", new BigDecimal(.5), 5);
		vendingMachine.addInventory("candy", new BigDecimal(.65), 5);

		assertEquals(new Integer(5), vendingMachine.checkInventoryCount("candy"));

		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.dime);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.dime);
		

		assertTrue(vendingMachine.selectProduct("cola"));
		assertEquals(decimalFormat.format(new BigDecimal(.7)), decimalFormat.format(vendingMachine.makeChange()));
		assertEquals(VendingMachine.THANK_YOU, vendingMachine.displayMessage());
		assertEquals(VendingMachine.INSERT_COIN, vendingMachine.displayMessage());
	}
	
	@Test
	public void selectAvailableInventoryProductWhenMoreThanRequiredCurrencyIsInsertedAndChangeDoesNotExist() {
		assertEquals(new Integer(2), vendingMachine.addCurrencyInventory(VendingMachine.quarter.getName(), VendingMachine.quarter, 2));
			
		vendingMachine.addInventory("cola", new BigDecimal(1), 5);
		vendingMachine.addInventory("chips", new BigDecimal(.5), 5);
		vendingMachine.addInventory("candy", new BigDecimal(.65), 5);

		assertEquals(new Integer(5), vendingMachine.checkInventoryCount("candy"));

		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.dime);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.dime);
		

		assertFalse(vendingMachine.selectProduct("cola"));
		assertEquals(decimalFormat.format(new BigDecimal(0)), decimalFormat.format(vendingMachine.makeChange()));
		assertEquals(VendingMachine.EXACT_CHANGE, vendingMachine.displayMessage());
	}
	
	@Test
	public void customerSelectsCurrencyDenominationReturn() {
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		
		assertTrue((new BigDecimal(1)).compareTo(vendingMachine.returnDepositedCurrencyDenomination()) == 0);
		assertEquals(VendingMachine.INSERT_COIN, vendingMachine.displayMessage());
	}
	
	@Test
	public void selectUnavialableInventoryWhenNoCurrencyIsInserted() {
		assertFalse(vendingMachine.selectProduct("cola"));
		assertEquals(VendingMachine.SOLD_OUT, vendingMachine.displayMessage());
		assertEquals(VendingMachine.INSERT_COIN, vendingMachine.displayMessage());
	}
	
	@Test
	public void selectUnavialableInventoryWhenCurrencyIsInserted() {
		vendingMachine.acceptCurrencyDenomination(VendingMachine.quarter);
		
		assertFalse(vendingMachine.selectProduct("cola"));
		assertEquals(VendingMachine.SOLD_OUT, vendingMachine.displayMessage());
		assertEquals(decimalFormat.format(VendingMachine.quarter.getValue()), vendingMachine.displayMessage());
	}
}
