package com.pillar.kata.vending;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KataVendingApplication {

	public static void main(String[] args) {
		SpringApplication.run(KataVendingApplication.class, args);
	}
}
