package com.pillar.kata.vending.domain;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class CurrencyDenomination {
	private String name;
	private Float weight;
	private Float diameter;
	private Float thickness;
	private BigDecimal value;
	
	public CurrencyDenomination() {
		super();
	}
	
	/*
	 * dimensions are in mm, and weight is in grams
	 */
	public CurrencyDenomination(String name, Float weight, Float diameter, Float thickness, BigDecimal value) {
		super();
		this.name = name;
		this.weight = weight;
		this.diameter = diameter;
		this.thickness = thickness;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Float getDiameter() {
		return diameter;
	}

	public void setDiameter(Float diameter) {
		this.diameter = diameter;
	}

	public Float getThickness() {
		return thickness;
	}

	public void setThickness(Float thickness) {
		this.thickness = thickness;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	@Override
	public int hashCode(){
	    return new HashCodeBuilder()
	        .append(weight)
	        .append(diameter)
	        .append(thickness)
	        .toHashCode();
	}

	@Override
	public boolean equals(final Object obj){
	    if(obj instanceof CurrencyDenomination){
	        final CurrencyDenomination other = (CurrencyDenomination) obj;
	        return new EqualsBuilder()
	            .append(weight, other.weight)
	            .append(diameter, other.diameter)
	            .append(thickness, other.thickness)
	            .isEquals();
	    } else{
	        return false;
	    }
	}
	
}
