package com.pillar.kata.vending.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Basic implementation of a vending machine.
 * 
 * Since this is just a basic example the majority of tools that would normally be employed
 * during actual implementation have been omitted: Spring IoC, i18n currency types, logging, 
 * message resources bundles, externalized configuration, service calls, persistence, etc. 
 *  
 * @author jason
 *
 */
public class VendingMachine {
	protected static String INSERT_COIN = "INSERT COIN";
	protected static String THANK_YOU = "THANK YOU";
	protected static String PRICE_PREFIX = "PRICE: ";
	protected static String SOLD_OUT = "SOLD OUT";
	protected static String EXACT_CHANGE = "EXACT CHANGE ONLY";

	protected String currentDisplayMessage = EXACT_CHANGE;

	protected static DecimalFormat decimalFormat = new DecimalFormat("#0.##");

	protected static CurrencyDenomination penny = new CurrencyDenomination("penny", 2.5f, 19.05f, 1.52f,
			new BigDecimal(.01).setScale(2, RoundingMode.HALF_UP));
	protected static CurrencyDenomination nickel = new CurrencyDenomination("nickel", 5f, 21.21f, 1.95f,
			new BigDecimal(.05).setScale(2, RoundingMode.HALF_UP));
	protected static CurrencyDenomination dime = new CurrencyDenomination("dime", 2.268f, 17.91f, 1.35f,
			new BigDecimal(.1).setScale(2, RoundingMode.HALF_UP));
	protected static CurrencyDenomination quarter = new CurrencyDenomination("quarter", 5.67f, 24.26f, 1.75f,
			new BigDecimal(.25).setScale(2, RoundingMode.HALF_UP));

	protected Map<String, InventoryItem> inventory = new HashMap<String, InventoryItem>();
	protected Map<String, CurrencyInventoryItem> currencyInventory = new HashMap<String, CurrencyInventoryItem>();
	protected List<CurrencyDenomination> validCurrencyDenominations = new ArrayList<CurrencyDenomination>();
	protected List<CurrencyDenomination> depositedCurrency = new ArrayList<CurrencyDenomination>();
	protected BigDecimal toReturnCurrency = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

	/*
	 * sets the valid currency denominations this machine accepts
	 */
	public VendingMachine() {
		validCurrencyDenominations.add(nickel);
		validCurrencyDenominations.add(dime);
		validCurrencyDenominations.add(quarter);
	}

	public Integer addInventory(String name, BigDecimal cost, Integer quantity) {
		inventory.put(name.toLowerCase(), new InventoryItem(cost, quantity));
		return quantity;
	}

	public Integer checkInventoryCount(String name) {
		if (inventory.containsKey(name.toLowerCase())) {
			return inventory.get(name.toLowerCase()).getQuantity();
		}

		return 0;
	}

	public Integer addCurrencyInventory(String name, Integer quantity, Float weight, Float diameter, Float thickness,
			BigDecimal value) {
		CurrencyInventoryItem currencyInventoryItem = new CurrencyInventoryItem(
				new CurrencyDenomination(name.toLowerCase(), weight, diameter, thickness, value), quantity);
		currencyInventory.put(name.toLowerCase(), currencyInventoryItem);

		currentDisplayMessage = INSERT_COIN;
		
		return currencyInventoryItem.getQuantity();
	}

	public Integer addCurrencyInventory(String name, CurrencyDenomination currencyDenomination, Integer quantity) {
		CurrencyInventoryItem currencyInventoryItem = new CurrencyInventoryItem(currencyDenomination, quantity);
		currencyInventory.put(name.toLowerCase(), currencyInventoryItem);

		currentDisplayMessage = INSERT_COIN;
		
		return currencyInventoryItem.getQuantity();
	}

	public Integer checkCurrencyDenominationInventoryCount(String name) {
		if (currencyInventory.containsKey(name.toLowerCase())) {
			return currencyInventory.get(name.toLowerCase()).getQuantity();
		}
		return 0;
	}

	protected void adjustCurrencyDenominationInventoryCount(String name, Integer adjustByQuantity) {
		if (currencyInventory.containsKey(name.toLowerCase())) {
			CurrencyInventoryItem item = currencyInventory.get(name.toLowerCase());
			item.setQuantity(item.getQuantity() + (adjustByQuantity));
		}

	}

	/*
	 * Very simple display states. A real implementation would probably utilize a more 
	 * sophisticated state mechanism
	 */
	public String displayMessage() {
		String returnMessage = currentDisplayMessage;

		if (currentDisplayMessage.equals(THANK_YOU)) {
			currentDisplayMessage = INSERT_COIN;
		}

		if (currentDisplayMessage.equals(SOLD_OUT)) {
			if (getDepositedCurrency().compareTo(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP)) > 0) {
				currentDisplayMessage = decimalFormat.format(getDepositedCurrency());
			} else {
				currentDisplayMessage = INSERT_COIN;
			}
		}

		return returnMessage;
	}

	protected BigDecimal getDepositedCurrency() {
		BigDecimal total = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
		for (CurrencyDenomination currencyDenomination : depositedCurrency) {
			total = total.add(currencyDenomination.getValue());
		}

		return total;
	}

	public boolean acceptCurrencyDenomination(CurrencyDenomination currencyDenomination) {
		if (validCurrencyDenominations.contains(currencyDenomination)) {
			depositedCurrency.add(currencyDenomination);
			currentDisplayMessage = decimalFormat.format(getDepositedCurrency());
			return true;
		} else {
			return false;
		}
	}

	protected InventoryItem getInventoryItem(String name) {
		if (inventory.containsKey(name.toLowerCase())) {
			return inventory.get(name.toLowerCase());
		} else {
			return null;
		}
	}

	protected void addDepositedCurrencyToCurrencyInventoryAndClearDeposit() {
		for (CurrencyDenomination currencyDenomination : depositedCurrency) {
			if (currencyInventory.containsKey(currencyDenomination.getName().toLowerCase())) {
				CurrencyInventoryItem currencyInventoryItem = currencyInventory.get(currencyDenomination.getName());
				currencyInventoryItem.setQuantity(currencyInventoryItem.getQuantity() + 1);
			} else {
				currencyInventory.put(currencyDenomination.getName().toLowerCase(),
						new CurrencyInventoryItem(currencyDenomination, 1));
			}
		}
	}

	public boolean selectProduct(String name) {
		if (checkInventoryCount(name) > 0) {
			InventoryItem item = getInventoryItem(name);

			if (getDepositedCurrency().compareTo(item.getCost()) == 0) {
				//exact change provided
				addDepositedCurrencyToCurrencyInventoryAndClearDeposit();
				currentDisplayMessage = THANK_YOU;
				return true;
			} else if (getDepositedCurrency().compareTo(item.getCost()) < 0) {
				// not enough change provided
				currentDisplayMessage = PRICE_PREFIX + decimalFormat.format(item.getCost());
				return false;
			} else {
				// more change than necessary
				BigDecimal difference = getDepositedCurrency().subtract(item.getCost()).setScale(2, RoundingMode.HALF_UP);
				if (determineChangeRequired(difference)) {
					addDepositedCurrencyToCurrencyInventoryAndClearDeposit();
					currentDisplayMessage = THANK_YOU;
					return true;
				} else {
					currentDisplayMessage = EXACT_CHANGE;
					return false;
				}
				
			} 
		} else {
			currentDisplayMessage = SOLD_OUT;

			return false;
		}
	}

	public BigDecimal returnDepositedCurrencyDenomination() {
		BigDecimal amountToReturn = getDepositedCurrency();
		depositedCurrency.clear();
		currentDisplayMessage = INSERT_COIN;
		return amountToReturn;
	}

	protected boolean determineChangeRequired(BigDecimal difference) {
		toReturnCurrency = difference;

		/*
		 * Simply calls determineChangeByDenomination with each denomination - whittling down 
		 * the existing difference to 0. If it can't do so then exact change is required.
		 */
		BigDecimal remainingDifference = determineChangeByDenomination(difference, quarter);
		remainingDifference = determineChangeByDenomination(remainingDifference, dime);
		remainingDifference = determineChangeByDenomination(remainingDifference, nickel);
		
		if (remainingDifference.compareTo(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP)) > 0) {
			currentDisplayMessage = EXACT_CHANGE;
			toReturnCurrency = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
			return false;
		}
		
		return true;
		
	}

	protected BigDecimal determineChangeByDenomination(BigDecimal difference, CurrencyDenomination currencyDenomination) {
		Integer qCount = difference.divide(currencyDenomination.getValue(), 2, RoundingMode.HALF_UP).intValue();
		for (int i = 1; i <= qCount.intValue(); i++) {
			if (checkCurrencyDenominationInventoryCount(currencyDenomination.getName()) >= (qCount - i)) {
				adjustCurrencyDenominationInventoryCount(currencyDenomination.getName(), -1);
				difference = difference.subtract(currencyDenomination.getValue());				
			}
		}
		
		return difference;
	}

	public BigDecimal makeChange() {
		return toReturnCurrency;
	}
}
