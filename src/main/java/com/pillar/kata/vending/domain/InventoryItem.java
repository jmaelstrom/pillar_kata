package com.pillar.kata.vending.domain;

import java.math.BigDecimal;

public class InventoryItem {
	private BigDecimal cost;
	private Integer quantity;
	
	public InventoryItem(BigDecimal cost, Integer quantity) {
		super();
		this.cost = cost;
		this.quantity = quantity;
	}
	public BigDecimal getCost() {
		return cost;
	}
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
}
