package com.pillar.kata.vending.domain;

public class CurrencyInventoryItem {
	private CurrencyDenomination currencyDenomination;
	private Integer quantity;
	
	public CurrencyInventoryItem(CurrencyDenomination currencyDenomination, Integer quantity) {
		super();
		this.currencyDenomination = currencyDenomination;
		this.quantity = quantity;
	}

	public CurrencyDenomination getCurrencyDenomination() {
		return currencyDenomination;
	}

	public void setCurrencyDenomination(CurrencyDenomination currencyDenomination) {
		this.currencyDenomination = currencyDenomination;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
	
}
